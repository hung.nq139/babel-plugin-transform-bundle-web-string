"use strict";

var browserify = require("browserify");
var babelify = require("babelify");
var fs = require("fs");

var b = browserify();

function endsWith(str, search) {
  return str.indexOf(search, str.length - search.length) !== -1;
}

module.exports = ({ types: t }) => {
  return {
    name: "transform-file-string",
    visitor: {
      ImportDeclaration: {
        exit: async (path, state) => {
          let arrPath = state.file.opts.filename.split("/");
          const fileName = arrPath.pop();
          const path = arrPath.join("/");

          if (endsWith(fileName, ".web.js")) {
            const fsStreamer = fs.createWriteStream(
              path + "/" + fileName + ".bundle.js"
            );
            b.add(path);
            b.transform(babelify, { presets: ["@babel/preset-env"] });
            b.bundle()
              .pipe(fsStreamer)
              .on("close", () => null);
          }
        }
      }

      // CallExpression: {
      //   exit: async (path, state) => {
      //     const callee = path.get("callee");
      //     if (callee.isIdentifier() && callee.equals("name", "require")) {
      //       const moduleArg = path.get("arguments")[0];
      //       const argValue = moduleArg.node.value;
      //       if (
      //         moduleArg &&
      //         moduleArg.isStringLiteral() &&
      //         endsWith(argValue, ".web")
      //       ) {
      //         if (path.parentPath.isVariableDeclarator()) {
      //           let arrPath = state.file.opts.filename.split("/");
      //           arrPath.pop();
      //           const cwd = arrPath.join("/");

      //           const codeStr = await getStr(cwd + "/" + argValue + ".js");
      //           console.log("codeStr2", codeStr);

      //           path.replaceWith(t.stringLiteral(codeStr));
      //         }
      //       }
      //     }
      //   }
      // }
    }
  };
};

// const getStr = async path => {
//   const puck = () => {
//     new Promise(resolve => {
//       b.add(path);
//       b.transform(babelify, { presets: ["@babel/preset-env"] });
//       b.bundle()
//         .pipe(fsStreamer)
//         .on("close", resolve);
//     });
//   };

//   await puck();
//   const p = babel.transformFileSync("./bundle.js", {
//     presets: ["minify"]
//   });

//   return p.code;
// };
